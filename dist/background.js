chrome.runtime.onInstalled.addListener(() => {
	chrome.action.disable();

	chrome.declarativeContent.onPageChanged.removeRules(undefined, () => {
		chrome.declarativeContent.onPageChanged.addRules([
			{
				conditions: [
					new chrome.declarativeContent.PageStateMatcher({
						pageUrl: {
							originAndPathMatches: 'https?://(?:.+\\.)?wowhead.com/(?:[a-z]+/)?(quest|mission|npc)=.*',
						}
					})
				],
				actions: [new chrome.declarativeContent.ShowAction()],
			}
		]);
	});
});

chrome.action.onClicked.addListener(tab => {
	chrome.scripting.insertCSS({
		target: { tabId: tab.id },
		files: [ "/page/page.css" ],
	}).then(() => {
		chrome.scripting.executeScript({
			target: { tabId: tab.id },
			files: [ "/page/page.js" ],
		});
	});
});
