import { $, $$ } from "./common/shortcuts";
import Parsers from "./Parsers";

// Remove any existing popups, if any
$$("#wet-popup").forEach(element => element.parentElement.removeChild(element));

// Create popup
let popup = document.createElement("div");
popup.setAttribute("id", "wet-popup");
popup.setAttribute("display", "none"); // starts hidden to avoid flicker
document.body.appendChild(popup);

popup.innerHTML = `
	<textarea id="wet-content" spellcheck="false"></textarea>
	<a id="wet-dark-toggle" href="#">Use dark theme</a>
	<button id="wet-copy">Copy to clipboard and close</button>
	<button id="wet-close">Close</button>
`;

let content = $("#wet-content");

$("#wet-copy").addEventListener("click", e => {
	content.select();
	document.execCommand("copy");
	popup.parentElement.removeChild(popup);
});

$("#wet-close").addEventListener("click", e => {
	popup.parentElement.removeChild(popup);
});

// Dark theme support

let darkThemeToggle = $("#wet-dark-toggle");
let isDark = false;

function setTheme() {
	if (isDark) {
		popup.classList.add("wet-dark");
		darkThemeToggle.textContent = "Use light theme";
	} else {
		popup.classList.remove("wet-dark");
		darkThemeToggle.textContent = "Use dark theme";
	}
}

if ("storage" in chrome) {
	chrome.storage.sync.get(["isDark"], result => {
		isDark = result.isDark !== false; // true if not set
		setTheme();
		popup.removeAttribute("display"); // show
	});
} else {
	setTheme();
	popup.removeAttribute("display"); // show
}

darkThemeToggle.addEventListener("click", () => {
	isDark = !isDark;
	setTheme();
	chrome.storage.sync.set({ "isDark": isDark });
});

// End dark theme support

Parsers.create().then(parsers => {
	let value = parsers.format();

	if (value) {
		content.value = value;
		$("#wet-copy").focus();
	}
}).catch(e => {
	content.value = e.message;
	console.log(e);
});
